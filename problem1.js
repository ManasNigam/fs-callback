const { rejects } = require("assert");
const fs = require("fs")
const path = require("path");
let newDirectory = "New-Directory"


let makeDirectoryAndRemoveFile = () => {

    let makeDirectory = new Promise((resolve, reject) => {
        let err = false;
        if (err) {
            reject("Error occured");
        }
        fs.mkdir(newDirectory, (err) => {
            if (err) {
                reject(err);
            }
            else {

                resolve("Directory created!");
            }
        })
    });

    let makeFilesInDirectory = () => {
        return new Promise((resolve, reject) => {

            let error = false;
            if (error) {
                reject("erroror");
            }

            else {
                var files = new Array(5).fill(0).map((items, index) => {
                    items = `${index + 1}.json`
                    return items;
                });

                for (let index = 0; index < files.length; index++) {

                    fs.writeFile(path.join(__dirname, newDirectory, files[index]), JSON.stringify("this is it"), function (error) {
                        if (error) {
                            reject("Error occured", error);
                        }
                        else {
                            setTimeout(() => {
                                fs.unlink(path.join(__dirname, newDirectory, files[index]), function (error) {
                                    if (error) {
                                        reject("File Already Deleted!");
                                    }
                                    else {
                                        resolve("All files created and deleted.")
                                    }
                                });
                            }, 3000);
                        }
                    });
                }
            }
        });
    };

    makeDirectory.then(result => {
        console.log(result);
        return makeFilesInDirectory();
    }).then(result => {
        console.log(result);
    }).catch(error => {
        console.log(error);
    });

}

module.exports = makeDirectoryAndRemoveFile;




