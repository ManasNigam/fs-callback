const fs = require("fs")
const path = require("path")

let solutionOfProblem2 = function () {

    fs.readFile(path.join(__dirname, "lipsum.txt"), function (err, data) {
        if (err) {
            console.error("Error occured : ", err);
        }
        else {
            console.log("File readed!");
            let upperCase = "upperCase.txt"
            fs.writeFile(upperCase, data.toString().toUpperCase(), function (err) {
                if (err) {
                    console.error("Error occured : ", err);

                }
                else {
                    console.log("Uppercase file created and data converted into uppercase!");
                    fs.writeFile(path.join(__dirname, "filenames.txt"), upperCase, function (err) {
                        if (err) {
                            console.error("Error occured : ", err);
                        } else {
                            console.log("File name added tp filenames.txt!");
                            fs.readFile(upperCase, function (err, data) {
                                if (err) {
                                    console.error("Error occured : ", err);
                                }
                                else {
                                    console.log("file readed!");
                                    let lowerCase = path.join(__dirname, "lowerCase.txt");
                                    fs.writeFile(lowerCase, data.toString().toLowerCase().split(".").join("\n"), function (err) {
                                        if (err) {
                                            console.error("Error occured : ", err);

                                        }
                                        else {
                                            console.log("Lowercase file created and data converted into lowercase!");
                                            fs.appendFile(path.join(__dirname, "filenames.txt"), "\nlowerCase.txt", function (err) {
                                                if (err) {
                                                    console.error("Error occured : ", err);
                                                }
                                                else {
                                                    fs.readFile(path.join(__dirname, "upperCase.txt"), function (err, data) {
                                                        if (err) {
                                                            console.error("Error occured");
                                                        }
                                                        else {
                                                            let sortedUpperCase = "sortedUpperCase.txt"
                                                            fs.writeFile(sortedUpperCase, data.toString(), function (err) {
                                                                if (err) {
                                                                    console.error("Error Occured while making sorted.sortedUpperCase.txt file!");
                                                                }
                                                                else {
                                                                    console.log("sortedUpperCase.txt created");
                                                                    fs.appendFile(path.join(__dirname, "filenames.txt"), "\nsortedUpperCase.txt", function () {
                                                                        if (err) {
                                                                            console.error("error occured while appending in filenames.txt");
                                                                        }
                                                                        else {
                                                                            console.log("name added to filenames.txt");
                                                                        }
                                                                    })

                                                                }
                                                            })


                                                        }
                                                    })

                                                    fs.readFile(path.join(__dirname, "lowerCase.txt"), function (err, data) {
                                                        if (err) {
                                                            console.error("Error occured");
                                                        }
                                                        else {
                                                            let sortedLowerCase = "sortedLowerCase.txt";
                                                            fs.writeFile(sortedLowerCase, data.toString().split(",").sort(), function (err) {
                                                                if (err) {
                                                                    console.error("Error Occured while making sorted.sortedUpperCase.txt file!");
                                                                }
                                                                else {
                                                                    console.log("sortedLowerCase.txt created");
                                                                    fs.appendFile(path.join(__dirname, "filenames.txt"), "\nsortedLowerCase.txt", function () {
                                                                        if (err) {
                                                                            console.error("error occured while appending in filenames.txt");
                                                                        }
                                                                        else {
                                                                            console.log("name added to filenames.txt");
                                                                            fs.readFile(path.join(__dirname, "filenames.txt"), function (err, data) {
                                                                                if (err) {

                                                                                }
                                                                                else {
                                                                                    let allFiles = data.toString().split("\n");
                                                                                    console.log(allFiles);
                                                                                    for (let index = 0; index < allFiles.length; index++) {
                                                                                        fs.unlink(path.join(__dirname, allFiles[index]), function (err) {
                                                                                            if (err) {
                                                                                                console.error("error occured while deleting");
                                                                                            }
                                                                                            else {
                                                                                                console.log(`File ${allFiles[index]} deleted successfully`);
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            })


                                                                        }
                                                                    })

                                                                }
                                                            })


                                                        }
                                                    })


                                                }
                                            })
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
            })
        }
    })
}


module.exports = solutionOfProblem2;